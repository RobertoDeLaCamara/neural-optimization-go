# NOG Service Repository


This is the main git repository of the **Neural Optimization Go Service**. 

**NOG** is a Hopfield ANN-based cloud-native service for solving optimization problems such as: 

* Shortest Path Problem
* Load Asignation Problem
* Vehicle Routing Problem
* ...

More info in: 

* [developer info](/doc/dev/README.md)
* [use cases](/doc/use_cases/README.md)


## Directories structure

* **helm**. Contains service's Helm charts.
* **ct**. Component test defined to test the service.
* **server**. Service source code
* **api**. OpenAPI for the service's RESTful interface.
* **neural**. Source code for the neural network and neural models.
* **ut**. Unit Test for the service.
* **doc**. Documentation.

[//]: # (EOF)

