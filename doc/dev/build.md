# Developer Information


## Build

Service's source code is located under `service` directory.
Unit tests' source code is located under `ut` directory.
Targets get compiled under `xxxx` directory....

The build system...

**Warning:** 


## Unit Test

Unit tests are *run* with `xxxxx`. You need to build them before.

## Packaging

### Docker Image

The service **must** be built before it is packaged.

The Dockerfile just copies the `neural-optimization-server` executable
and declares it as the entry point.

```
$ xxxxxx

```
It is advisable to run the container in detached mode and provide a name to the container. Be aware of the port binding.

```
$ docker run --rm -d -p 1970:3000 --name xxxxx


```

### Docker Image


### Docker registry



