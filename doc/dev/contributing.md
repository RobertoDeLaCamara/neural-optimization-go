# Contributing to NOG 


## Reviewing and submitting
TBD

## About changes and commits

* Commits should be:
  * Small.
  * Separated (don't put two different things in the same change).

### Commit message

* Detailed description.


[//]: # (EOF)