/*
 * Copyright (c) 2018.  Roberto de la Cámara (roberto.de.la.camara.garcia@gmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 *
 */

package main

import (
	"log"
	"net/http"
	sw "gitlab.com/RobertoDeLaCamara/neural-optimization-go/server"
)

func main() {
	log.Printf("NOG server started")
	router := sw.NewRouter()
	log.Fatal(http.ListenAndServe(":1970", router))
}
