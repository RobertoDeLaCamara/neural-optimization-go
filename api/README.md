# OpenAPI for the NOG Service
 
API file compliant to Swagger '2.0' specification. 

More info in [Swagger](https://swagger.io/).