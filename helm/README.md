# Helm Chart for the NOG Service

Resources to deploy NOG Service using Helm.
Each chart is a directory under /helm directory.

## Deploy and check:

1. Install the chart:

    `helm install xxxx`

2. Check the status:

    `helm status xxxx`

## Upload to ?? helm repository 
